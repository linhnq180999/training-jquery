
var data = JSON.parse(dataJson);
var user = data.users
var positionData = data.positions

$(document).ready(function () {

  $('#tableData').DataTable({
    data: user,
    select: true,
    paging: false,
    searching: false,
    columns: [
      {
        data: "id",
        id: "id"
      },
      { data: "name" },
      {
        data: null,
        bSortable: false,
        mRender:
          function (o) {
            return '<select class="form-control" id="positionTable' + o.id + '">' +
              '<option value="1">System Architect</option>' +
              '<option  value="2">Customer Support</option>' +
              '<option value="3">Javascript Developer</option>' +
              '<option value="4">Regional Director</option>' +
              '</select>'
          }
      },
      {
        data: null,
        bSortable: false,
        mRender: function (value) {
          return '<select class="form-control" id="officesTable' + value.id + '">' +
            '<option value="1">New York</option>' +
            '<option  value="2">Argentina</option>' +
            '<option value="3">Edinburgh</option>' +
            '<option value="4">San Francisco</option>' +
            '<option value="5">London</option>' +
            '</select>'
        }
      },
      {
        data: null,
        bSortable: false,
        mRender: function (value) {
          return '<input type="text" class="form-control" id="salarayTable' + value.id + '" />'
        }
      },
      {
        data: null,
        bSortable: false,
        mRender: function (value) {
          return '<input type="date" class="form-control" id="startDateTable' + value.id + '" />'
        }
      },
      {
        data: null,
        bSortable: false,
        mRender: function (o) {
          return '<button type="button" class="btn btn-danger" onClick="deleteValue(' + o.id + ')" >' + 'delete' + '</button>' + '  '
            ;
        }
      }
    ]
  });

  $(function () {
    for (let index = 0; index < user.length; index++) {
      var positionTable = '#positionTable' + index
      var officeTable = '#officesTable' + index
      var salarayTable = '#salarayTable' + index
      var startDateTable = '#startDateTable' + index
      user.filter(function (obj) {
        if (index == obj.id) {
          $(positionTable).val(obj.position).prop('selected', true);
          $(officeTable).val(obj.office).prop('selected', true);
          $(salarayTable).val(obj.salary);
          $(startDateTable).val(obj.start_date);
        }
      });
    }
  });

  $("#tableData tbody").on('click', 'tr', function () {
    $('#tableData tbody tr.selected').removeClass('selected');
    $(this).addClass("selected");
    let dataSelected = $('#tableData').DataTable().row(this).data();
    detail(dataSelected.id)
  });

  $("#searchForm").click(function () {
    var idSearch = $('#idSearch').val();
    var value = $('#nameSearch').val();
    var searchValue;
    if (idSearch, value) {
      searchValue = user.filter(function (obj) {
        return (obj.id == idSearch) && (obj.name == value);
      });
    } else if (idSearch) {
      searchValue = user.filter(function (obj) {
        return obj.id == idSearch;
      });
    } else if (value) {
      searchValue = user.filter(function (obj) {
        return obj.name == value;
      });
    } else {
      searchValue = user
    }
    $('#tableData').dataTable().fnClearTable();
    $('#tableData').dataTable().fnAddData(searchValue);
  });

  $('#clearForm').click(function () {
    return ($('#id').val('')) && ($('#name').val('')) && ($('#position').val(0) && ($('#salary').val('')));
  });

  $('#submitForm').click(function () {
    var dataTable = $('#tableData').DataTable().rows().data();
    var submitForm = dataTable.map(item => {
      return {
        id: item.id,
        name: item.name,
        position: ($('#positionTable' + item.id).val()),
        office: ($('#officesTable' + item.id).val()),
        salary: ($('#salarayTable' + item.id).val()),
        start_date: ($('#startDateTable' + item.id).val()),
      }
    });
    console.warn(submitForm);
  });

});

function deleteValue(id) {
  var removeIndex = user.map(item => item.id).indexOf(id);
  removeData = user.splice(removeIndex, 1);
  console.warn(removeData);
}

function detail(id) {
  filterData = user.filter(function (obj) {
    return obj.id == id;
  });
  filterData.map(item => {
    return ($('#id').val(item.id)) && ($('#name').val(item.name)) && ($('#position').val(item.position) && ($('#salary').val(item.salary)));
  })
}
